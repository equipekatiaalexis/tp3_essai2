__author__ = 'Jean-Francis Roy'

from piece import Piece
from position import Position

class Damier:
    """Plateau de jeu d'un jeu de dames. Contient un ensemble de pièces positionnées à une certaine position
    sur le plateau.

    Attributes:
        cases (dict): Dictionnaire dont une clé représente une Position, et une valeur correspond à la Piece
            positionnée à cet endroit sur le plateau.

        n_lignes (int): Le nombre de lignes du plateau. La valeur est 8 (constante).
        n_colonnes (int): Le nombre de colonnes du plateau. La valeur est 8 (constante).

    """

    def __init__(self):
        """Constructeur du Damier. Initialise un damier initial de 8 lignes par 8 colonnes.

        """
        self.n_lignes = 8
        self.n_colonnes = 8

        self.cases = {
            Position(7, 0): Piece("blanc", "pion"),
            Position(7, 2): Piece("blanc", "pion"),
            Position(7, 4): Piece("blanc", "pion"),
            Position(7, 6): Piece("blanc", "pion"),
            Position(6, 1): Piece("blanc", "pion"),
            Position(6, 3): Piece("blanc", "pion"),
            Position(6, 5): Piece("blanc", "pion"),
            Position(6, 7): Piece("blanc", "pion"),
            Position(5, 0): Piece("blanc", "pion"),
            Position(5, 2): Piece("blanc", "pion"),
            Position(5, 4): Piece("blanc", "pion"),
            Position(5, 6): Piece("blanc", "pion"),
            Position(2, 1): Piece("noir", "pion"),
            Position(2, 3): Piece("noir", "pion"),
            Position(2, 5): Piece("noir", "pion"),
            Position(2, 7): Piece("noir", "pion"),
            Position(1, 0): Piece("noir", "pion"),
            Position(1, 2): Piece("noir", "pion"),
            Position(1, 4): Piece("noir", "pion"),
            Position(1, 6): Piece("noir", "pion"),
            Position(0, 1): Piece("noir", "pion"),
            Position(0, 3): Piece("noir", "pion"),
            Position(0, 5): Piece("noir", "pion"),
            Position(0, 7): Piece("noir", "pion"),
        }

    def recuperer_piece_a_position(self, position):
        """Récupère une pièce dans le damier à partir d'une position.

        Args:
            position (Position): La position où récupérer la pièce.

        Returns:
            La pièce (de type Piece) à la position reçue en argument, ou None si aucune pièce n'était à cette position.

        """
        'Création et assignation des variables de départ'

        str_piece = Damier().cases[Position(position['ligne'], position['colonne'])] #Aller chercher la pièce à l'endroit

        'Retour des variables'
        return(str_piece)


    def position_est_dans_damier(self, position):
        """Vérifie si les coordonnées d'une position sont dans les bornes du damier (entre 0 inclusivement et le nombre
        de lignes/colonnes, exclusement.

        Args:
            position (Position): La position à valider.

        Returns:
            bool: True si la position est dans les bornes, False autrement.

        """

        #Définition des variables de départ
        bol_verification_format = False #Variable qui permet de sortir de la boucle lorsqu'elle est true
        lst_coord_dep = position #Variable de position de la piece
        lst_chiffres_valide = [0,1,2,3,4,5,6,7] #Liste des valeurs X&Y pour lesquelles on est dans le damier

        #Vérification du format des coordonnées à l'aide d'un "try".
        while bol_verification_format == False:
            #Vérification de la validité du format des coordonnées individuelles. Spécifiquement si la pièce est dans le damier
            if int(lst_coord_dep['ligne']) not in lst_chiffres_valide: #Si X n'est pas dans le damier
                return(False)
            if int(lst_coord_dep['colonne']) not in lst_chiffres_valide: #Si Y n'est pas dans le damier
                return(False)
            #Si tout est passé, le format est bon
            return(True)

    def piece_peut_se_deplacer_vers(self, position_piece, position_cible):
        """Cette méthode détermine si une pièce (à la position reçue) peut se déplacer à une certaine position cible.
        On parle ici d'un déplacement standard (et non une prise).

        Une pièce doit être positionnée à la position_piece reçue en argument (retourner False autrement).

        Une pièce de type pion ne peut qu'avancer en diagonale (vers le haut pour une pièce blanche, vers le bas pour
        une pièce noire). Une pièce de type dame peut avancer sur n'importe quelle diagonale, peu importe sa couleur.
        Une pièce ne peut pas se déplacer sur une case déjà occupée par une autre pièce. Une pièce ne peut pas se
        déplacer à l'extérieur du damier.

        Args:
            self : Partie
            position_piece (Position): La position de la pièce source du déplacement.
            position_cible (Position): La position cible du déplacement.

        Returns:
            bool: True si la pièce peut se déplacer à la position cible, False autrement.

        """

        'Déclaration de variables'
        Partie = self
        dic_pos_dep = position_piece #Variable de la position de départ
        dic_pos_fin = position_cible #Variable de la position de fin
        bol_pion = Piece.est_pion(dic_pos_dep) #Vérification s'il s'agit d'un pion
        bol_noir = Piece.est_noire(dic_pos_dep) #Vérification s'il s'agit d'une pièce noire

        'Assignation des positions possibles pour une pièce en particulier'
        if bol_pion == True:
            if bol_noir == True: #Si pion noir
                dic_positions_possible = Position.positions_diagonales_bas(dic_pos_dep)
            else: #Si pion blanc
                dic_positions_possible = Position.positions_diagonales_haut(dic_pos_dep)
        else: #Si dame
            dic_positions_possible = Position.quatre_positions_diagonales(dic_pos_dep)
        
        'Itération de vérification que les positions possibles existent dans le damier'
        for n in dic_positions_possible: #Pour chacune des positions trouvées
            dic_pos_courante = {} #Déclaration du dictionaire
            dic_pos_courante = dic_positions_possible[n] #Assignation des coordonnées X,Y la position courante à une dice
            bol_existence_case = Damier.position_est_dans_damier(Partie, dic_pos_courante) #Vérification de l'existence de la case
            if bol_existence_case == False: #Si la case n'existe pas
                dic_positions_possible.remove[n] #Enlever celle ci des solutions possibles
        
        'Itération de vérification des positions finales'
        for n in dic_positions_possible: #Pour chacune des positions trouvées
            dic_pos_courante = {} #Déclaration du dictionaire
            dic_pos_courante = dic_positions_possible[n] #Assignation des coordonnées X,Y la position courante à une dice
            str_type_piece = Damier.recuperer_piece_a_position(Partie, dic_pos_courante)
            if str_type_piece != "": #Si l'endroit pour se déplacer n'est pas vide
                dic_positions_possible.remove[n]

        'Vérification si la pièce peut faire une prise'
        
        'Vérification que la position cible est dans les positions restantes et retours'
        if dic_pos_fin in dic_positions_possible:
            return(True)
        else:
            return(False)
        
        
    def piece_peut_sauter_vers(self, position_piece, position_cible):
        """Cette méthode détermine si une pièce (à la position reçue) peut sauter vers une certaine position cible.
        On parle ici d'un déplacement qui "mange" une pièce adverse.

        Une pièce doit être positionnée à la position_piece reçue en argument (retourner False autrement).

        Une pièce ne peut que sauter de deux cases en diagonale. N'importe quel type de pièce (pion ou dame) peut sauter
        vers l'avant ou vers l'arrière. Une pièce ne peut pas sauter vers une case qui est déjà occupée par une autre
        pièce. Une pièce ne peut faire un saut que si elle saute par dessus une pièce de couleur adverse.

        Args:
            position_piece (Position): La position de la pièce source du saut.
            position_cible (Position): La position cible du saut.

        Returns:
            bool: True si la pièce peut sauter vers la position cible, False autrement.

        """
        #TODO: À compléter

    def piece_peut_se_deplacer(self, position_piece):
        """Vérifie si une pièce à une certaine position a la possibilité de se déplacer (sans faire de saut).

        ATTENTION: N'oubliez pas qu'étant donné une position, il existe une méthode dans la classe Position retournant
        les positions des quatre déplacements possibles.

        Args:
            position_piece (Position): La position source.

        Returns:
            bool: True si une pièce est à la position reçue et celle-ci peut se déplacer, False autrement.

        """

        'Déclaration des variables de départ'
        Partie = self
        lst_pos_dep = position_piece #Variable de position de la piece
        piece_depart = Damier.recuperer_piece_a_position(self, lst_pos_dep)
        bol_pion = Piece.est_pion(piece_depart) #Vérification s'il s'agit d'un pion
        bol_noir = Piece.est_noire(piece_depart) #Vérification s'il s'agit d'une pièce noire

        'Assignation des positions possibles selon les conditions de pièces et de couleur'
        if bol_pion == True:
            if bol_noir == True: #Si pion noir
                lst_positions_possible = Position.positions_diagonales_bas(lst_pos_dep)
            else: #Si pion blanc
                lst_positions_possible = Position.positions_diagonales_haut(lst_pos_dep)
        else: #Si dame
            lst_positions_possible = Position.quatre_positions_diagonales(lst_pos_dep)

        'Itération de vérification que les positions possibles existent dans le damier'
        for n in lst_positions_possible: #Pour chacune des positions trouvées
            dic_pos_courante = {} #Déclaration du dictionaire
            dic_pos_courante = lst_positions_possible[n] #Assignation des coordonnées X,Y la position courante à une liste
            bol_existence_case = Damier.position_est_dans_damier(Partie, dic_pos_courante) #Vérification de l'existence de la case
            if bol_existence_case == False: #Si la case n'existe pas
                lst_positions_possible.remove[n] #Enlever celle ci des solutions possibles

        'Itération de vérification des positions finales'
        for n in lst_positions_possible: #Pour chacune des positions trouvées
            dic_pos_courante = {} #Déclaration du dictionaire
            dic_pos_courante = lst_positions_possible[n] #Assignation des coordonnées X,Y la position courante à une liste
            str_type_piece = Damier.recuperer_piece_a_position(Partie, dic_pos_courante)
            if str_type_piece == "": #S'il y a un endroit libre ou la pièce peut se déplacer
                return(True)

        #Si les tests supérieurs ont échoués, il n'y a pas de déplacements possibles
        return(False)

    def piece_peut_faire_une_prise(self, position_piece):
        """Vérifie si une pièce à une certaine position a la possibilité de faire une prise.

        ATTENTION: N'oubliez pas qu'étant donné une position, il existe une méthode dans la classe Position retournant
        les positions des quatre sauts possibles.

        Args:
            position_piece (Position): La position source.

        Returns:
            bool: True si une pièce est à la position reçue et celle-ci peut faire une prise. False autrement.

        """
        #TODO: À compléter

    def piece_de_couleur_peut_se_deplacer(self, couleur):
        """Vérifie si n'importe quelle pièce d'une certaine couleur reçue en argument a la possibilité de se déplacer
        vers une case adjacente (sans saut).

        ATTENTION: Réutilisez les méthodes déjà programmées!

        Args:
            couleur (str): La couleur à vérifier.

        Returns:
            bool: True si une pièce de la couleur reçue peut faire un déplacement standard, False autrement.
        """
        #TODO: À compléter

    def piece_de_couleur_peut_faire_une_prise(self, couleur):
        """Vérifie si n'importe quelle pièce d'une certaine couleur reçue en argument a la possibilité de faire un
        saut, c'est à dire vérifie s'il existe une pièce d'une certaine couleur qui a la possibilité de prendre une
        pièce adverse.

        ATTENTION: Réutilisez les méthodes déjà programmées!

        Args:
            couleur (str): La couleur à vérifier.

        Returns:
            bool: True si une pièce de la couleur reçue peut faire un saut (une prise), False autrement.
        """
        #TODO: À compléter

    def deplacer(self, position_source, position_cible):
        """Effectue le déplacement sur le damier. Si le déplacement est valide, on doit mettre à jour le dictionnaire
        self.cases, en déplaçant la pièce à sa nouvelle position (et possiblement en supprimant une adverse pièce qui a
        été prise).

        Cette méthode doit également:
        - Promouvoir un pion en dame si celui-ci atteint l'autre extrémité du plateau.
        - Retourner un message indiquant "ok", "prise" ou "erreur".

        ATTENTION: Si le déplacement est effectué, cette méthode doit retourner "ok" si aucune prise n'a été faite,
            et "prise" si une pièce a été prise.
        ATTENTION: Ne dupliquez pas de code! Vous avez déjà programmé (ou allez programmer) des méthodes permettant
            de valider si une pièce peut se déplacer vers un certain endroit ou non.

        Args:
            position_source (Position): La position source du déplacement.
            position_cible (Position): La position cible du déplacement.

        Returns:
            str: "ok" si le déplacement a été effectué sans prise, "prise" si une pièce adverse a été prise, et
                "erreur" autrement.

        """
        #TODO: À compléter

    def convertir_en_chaine(self):
        """Retourne une chaîne de caractères où chaque case est écrite sur une ligne distincte.
        Chaque ligne contient l'information suivante (respectez l'ordre et la manière de séparer les éléments):
        ligne,colonne,couleur,type

        Par exemple, un damier à deux pièces (un pion noir en (1, 2) et une dame blanche en (6, 1)) serait représenté
        par la chaîne suivante:

        1,2,noir,pion
        6,1,blanc,dame

        Cette méthode pourrait par la suite être réutilisée pour sauvegarder un damier dans un fichier.

        Returns:
            (str): La chaîne de caractères construite.

        """
        #TODO: À compléter

    def charger_dune_chaine(self, chaine):
        """Vide le damier actuel et le remplit avec les informations reçues dans une chaîne de caractères. Le format de
        la chaîne est expliqué dans la documentation de la méthode convertir_en_chaine.

        Args:
            chaine (str): La chaîne de caractères.

        """
        #TODO: À compléter

    def __repr__(self):
        """Cette méthode spéciale permet de modifier le comportement d'une instance de la classe Damier pour
        l'affichage. Faire un print(un_damier) affichera le damier à l'écran.

        """
        s = " +-0-+-1-+-2-+-3-+-4-+-5-+-6-+-7-+\n"
        for i in range(0, 8):
            s += str(i)+"| "
            for j in range(0, 8):
                if Position(i, j) in self.cases:
                    s += str(self.cases[Position(i, j)])+" | "
                else:
                    s += "  | "
            s += "\n +---+---+---+---+---+---+---+---+\n"

        return s


if __name__ == "__main__":
    # Ceci n'est pas le point d'entrée du programme principal, mais il vous permettra de faire de petits tests avec
    # vos fonctions du damier.
    damier = Damier()
    print(damier)
