__author__ = 'Jean-Francis Roy'
from damier import Damier
from position import Position
import damier

class Partie:
    """Gestionnaire de partie de dames.

    Attributes:
        damier (Damier): Le damier de la partie, contenant notamment les pièces.
        couleur_joueur_courant (str): Le joueur à qui c'est le tour de jouer.
        doit_prendre (bool): Un booléen représentant si le joueur actif doit absoluement effectuer une prise
            de pièce. Sera utile pour valider les mouvements et pour gérer les prises multiples.
        position_source_selectionnee (Position): La position source qui a été sélectionnée. Utile pour sauvegarder
            cette information avant de poursuivre. Contient None si aucune pièce n'est sélectionnée.
        position_source_forcee (Position): Une position avec laquelle le joueur actif doit absoluement jouer. Le
            seul moment où cette position est utilisée est après une prise: si le joueur peut encore prendre
            d'autres pièces adverses, il doit absolument le faire. Ce membre contient None si aucune position n'est
            forcée.

    """
    def __init__(self):
        """Constructeur de la classe Partie. Initialise les attributs à leur valeur par défaut. Le damier est construit
        avec les pièces à leur valeur initiales, le joueur actif est le joueur blanc, et celui-ci n'est pas forcé
        de prendre une pièce adverse. Aucune position source n'est sélectionnée, et aucune position source n'est forcée.

        """
        self.damier = Damier()
        self.couleur_joueur_courant = "blanc"
        self.doit_prendre = False
        self.position_source_selectionnee = None
        self.position_source_forcee = None

    def verification_format(self, chaine_position):
        """

        :param chaine_position:
        :return (Si bon format, liste de coordonées)
        """
        'Délaration et assignation de variable'
        bol_verification_format = False #Variable qui permet de sortir de la boucle lorsqu'elle est true
        str_coord_dep_liste = chaine_position #Variable de position de la piece
        n = 0

        while bol_verification_format == False:
            try:
                #Vérification du format général
                lst_coord_dep = str_coord_dep_liste.split(",")
                #Si tout est passé, le format est bon, Conversion en entiers
                while n < 2 : #Vérification que les coordonnées sont des entiers
                    test_entier = float(lst_coord_dep[n]) - int(lst_coord_dep[n])
                    if test_entier != float(0):
                        return(False, "")
                    n = n+1
                lst_coord_dep[0] = int(lst_coord_dep[0])
                lst_coord_dep[1] = int(lst_coord_dep[1])
                dic_coord_dep = {'ligne': lst_coord_dep[0], 'colonne': lst_coord_dep[1]}
                return(True, dic_coord_dep)
            except: #S'il y a une erreur (Principalement dans le format d'entrée)
                return(False, "")

    def position_source_valide(self, position_source):
        """Vérifie la validité de la position source, notamment:
            - Est-ce que la position contient une pièce?
            - Est-ce que cette pièce est de la couleur du joueur actif?
            - Si le joueur doit absoluement continuer son mouvement avec une prise supplémentaire, a-t-il choisi la
              bonne pièce?

        Cette méthode retourne deux valeurs. La première valeur est Booléenne (True ou False), et la seconde valeur est
        un message d'erreur indiquant la raison pourquoi la position n'est pas valide (ou une chaîne vide s'il n'y a pas
        d'erreur).

        ATTENTION: Utilisez les attributs de la classe pour connaître les informations sur le jeu! (le damier, le joueur
            actif, si une position source est forcée, etc.

        ATTENTION: Vous avez accès ici à un attribut de type Damier. vous avez accès à plusieurs méthodes pratiques
            dans le damier qui vous simplifieront la tâche ici :)

        Args:
            position_source (Position): La position source à valider.

        Returns:
            bool, str: Un couple où le premier élément représente la validité de la position (True ou False), et le
                 deuxième élément est un message d'erreur (ou une chaîne vide s'il n'y a pas d'erreur).

        """

        #Assignation de variables de départ
        lst_pos_dep = position_source

        """ Vérifications à la de fonctions dans damier.py"""

        #Vérification du format ainsi que des coordonnées en général
        bol_verification_damier = Damier.position_est_dans_damier(damier, lst_pos_dep)

        #Vérification de l'existence d'une pièce à la position et que la pièce choisie est au joueur en cours.
        str_type_piece = Damier.recuperer_piece_a_position(Partie, lst_pos_dep)
        if str_type_piece == "":
            return(False,"Il n'y a pas de pièce à cette position")

        #Vérification de l'appartenance de la pièce au joueur
        if self.couleur_joueur_courant == "blanc":
            if str_type_piece == "x" or str_type_piece == "X":
                return(False,"Cette pièce n'appartient pas à ce joueur")
        elif self.couleur_joueur_courant == "noir":
            if str_type_piece == "o" or str_type_piece == "O":
                return(False,"Cette pièce n'appartient pas à ce joueur")

        #Vérification qu'il existe des déplacements possibles pour la pièce choisie
        testtest = Damier.piece_peut_se_deplacer(Partie, lst_pos_dep)

        return(True,"")
        #TODO: A finir

    def position_cible_valide(self, position_cible):
        """Vérifie si la position cible est valide (en fonction de la position source sélectionnée). Doit non seulement
        vérifier si le déplacement serait valide (utilisez les méthodes que vous avez programmées dans le Damier!), mais
        également si le joueur a respecté la contrainte de prise obligatoire.

        Returns:
            bool, str: Deux valeurs, la première étant Booléenne et indiquant si la position cible est valide, et la
                seconde valeur est une chaîne de caractères indiquant un message d'erreur (ou une chaîne vide s'il n'y
                a pas d'erreur).

        """
        #TODO: À compléter

    def demander_positions_deplacement(self):
        """Demande à l'utilisateur les positions sources et cible, et valide ces positions. Cette méthode doit demander
        les positions à l'utilisateur tant que celles-ci sont invalides.

        Cette méthode ne doit jamais planter, peu importe ce que l'utilisateur entre. Voir le module 4.5 pour des
        trucs pour vérifier si une information fournie est un entier ou non.

        Returns:
            Position, Position: Un couple de deux positions (source et cible).

        """

        """ Définition des variables de départ"""
        #Assignation de la couleur du joueur en cours à la variable str_joueur_en_cours
        str_joueur_en_cours = str(self.couleur_joueur_courant)
        if str_joueur_en_cours == "blanc":
            str_joueur_en_cours = str_joueur_en_cours + " (O)"
        elif str_joueur_en_cours == "noir":
            str_joueur_en_cours = str_joueur_en_cours + " (X)"

        """ Instructions pour l'utilisateur """
        #Définition des instructions avant le damier
        str_tour = "C'est au tour du joueur " + str_joueur_en_cours + " à jouer"
        str_montrer = "Voici le damier actuel :"
        str_instructions = str_tour + "\n" + str_montrer

        #Imprimer les instructions avec le Damier
        self.imprimer(str_instructions)

        #Définition des instructions apres le damier
        str_demande = "Veuillez entrer les coordonnées (lignes, colonnes) de la pièce que vous voulez déplacer sous le format ligne, colenne (Exemple: 5,2)"

        """ Demande et vérification des coordonnées"""

        #Vérification du format
        bol_ver_coord_dep = False #Déclaration de la variable servant à sortir de l'itération
        while bol_ver_coord_dep == False:
            str_coord_dep = input(str_demande) #Demande la coordonnée
            lst_verification_format = self.verification_format(str_coord_dep) #Vérification du format des coordonnées
            if lst_verification_format[0] == True: #Si les coordonnées sont sous le bon format
                lst_coord_dep = lst_verification_format[1] #Assignation des coordonnées sous forme de liste a une variable
                lst_ver_coord_dep = self.position_source_valide(lst_coord_dep) #Vérification que la position source est valide
                #Assignation des données de la liste à des variables différentes dont bol_ver_coord_dep qui peut terminer la boucle
                bol_ver_coord_dep = lst_ver_coord_dep[0] #Sort de la boucle maintenant si tout s'est bien passé
                str_ver_coord_dep = lst_ver_coord_dep[1]
                #Impression du message d'erreur
                print(str_ver_coord_dep)
            else:
                print("Coordonnées sous format non-valide. Veuillez rentrer les coordonnées sous format X,Y (ex: 5,2)")

        #TODO: A finir

    def tour(self):
        """Cette méthode effectue le tour d'un joueur, et doit effectuer les actions suivantes:
        - Assigne self.doit_prendre à True si le joueur courant a la possibilité de prendre une pièce adverse.
          (n'oubliez pas que vous avez accès à des méthodes dans la classe Position et la classe Damier!)
        - Demander les positions source et cible (utilisez self.demander_positions_deplacement!)
        - Effectuer le déplacement (à l'aide de la méthode du damier appropriée)
        - Si une pièce a été prise lors du déplacement, c'est encore au tour du même joueur si celui-ci peut encore
          prendre une pièce adverse en continuant son mouvement. Utilisez les membres self.doit_prendre et
          self.position_source_forcee pour forcer ce prochain tour!
        - Si aucune pièce n'a été prise ou qu'aucun coup supplémentaire peut être fait avec la même pièce, c'est le
          tour du joueur adverse. Mettez à jour les attributs de la classe en conséquence.

        """
        self.demander_positions_deplacement()

        #TODO: A finir

    def imprimer(self, phrase):

        print("\n" * 100)
        print(phrase)
        print(Damier())

    def jouer(self):
        """Démarre une partie. Tant que le joueur courant a des déplacements possibles (utilisez les méthodes
        appriopriées!), un nouveau tour est joué.

        Returns:
            str: La couleur du joueur gagnant.
        """

        self.tour()
        #TODO: A finir

    def sauvegarder(self, nom_fichier):
        """Sauvegarde une partie dans un fichier. Le fichier contiendra:
        - Une ligne indiquant la couleur du joueur courant.
        - Une ligne contenant True ou False, si le joueur courant doit absolument effectuer une prise à son tour.
        - Une ligne contenant None si self.position_source_forcee est à None, et la position ligne,colonne autrement.
        - Le reste des lignes correspondent au damier. Voir la méthode convertir_en_chaine du damier pour le format.

        ATTENTION: Lorsqu'on écrit ou lit dans un fichier texte, il faut s'assurer de bien convertir les variables
        dans le bon type.

        Exemple de contenu de fichier :

        blanc
        True
        6,1
        1,2,noir,dame
        1,6,blanc,pion
        4,1,noir,dame
        5,2,noir,pion
        6,1,blanc,dame


        Args:
            nom_fichier (str): Le nom du fichier où sauvegarder.

        """
        #TODO: À compléter

    def charger(self, nom_fichier):
        """Charge une partie à partir d'un fichier. Le fichier a le même format que la méthode de sauvegarde.

        ATTENTION: N'oubliez pas de bien convertir les chaînes de caractères lues!

        Args:
            nom_fichier (str): Le nom du fichier à lire.

        """
        #TODO: À compléter


if __name__ == "__main__":

    # Point d'entrée du programme. On initialise une nouvelle partie, et on appelle la méthode jouer().

    print("Bienvenue dans le programme du jeu de dames simplifié")
    partie = Partie()
    partie.jouer()

    # Si on veut sauvegarder une partie.
    #partie.sauvegarder("ma_partie.txt")

    # Si on veut charger un fichier.
    #partie.charger("exemple_partie.txt")

    # Note: Nous ne vous demandons pas d'inclure les fonctionnalités de sauvegarde et de chargement dans le menu de jeu.
    # Par contre, assurez-vous que vos méthodes fonctionnent en les testant! Ces méthodes seront réutilisées dans le
    # TP4.

    """
    gagnant = partie.jouer()

    print("------------------------------------------------------")
    print("Partie terminée! Le joueur gagnant est le joueur", gagnant)
    """