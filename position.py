__author__ = 'Jean-Francis Roy'


class Position:
    """Une position à deux coordonnées: ligne et colonne. La convention utilisée est celle de la notation matricielle :
    le coin supérieur gauche d'une matrice est dénoté (0, 0) (ligne 0 et colonne 0). On additionne une unité de colonne
    lorsqu'on se déplace vers la droite, et une unité de ligne lorsqu'on se déplace vers le bas.

    +-------+-------+-------+-------+
    | (0,0) | (0,1) | (0,2) |  ...  |
    | (1,0) | (1,1) | (1,2) |  ...  |
    | (2,0) | (2,1) | (2,2) |  ...  |
    |  ...  |  ...  |  ---  |  ...  |
    +-------+-------+-------+-------+

    Attributes:
        ligne (int): La ligne associée à la position.
        colonne (int): La colonne associée à la position

    """
    def __init__(self, ligne, colonne):
        """Constructeur de la classe Position. Initialise les deux attributs de la classe.

        Args:
            ligne (int): La ligne à considérer dans l'instance de Position.
            colonne (int): La colonne à considérer dans l'instance de Position.

        """
        self.ligne = int(ligne)
        self.colonne = int(colonne)

    def positions_diagonales_bas(self):
        """Retourne une liste contenant les deux positions diagonales bas à partir de la position actuelle.

        Note:
            Dans cette méthode et les prochaines, vous n'avez pas à valider qu'une position est "valide", car dans le
            contexte de cette classe toutes les positions (même négatives) sont permises.

        Returns:
            list: La liste des deux positions.

        """

        'Déclaration des variables'
        #Variables de la position de la piece de depart
        int_pos_dep_lig = self['ligne']
        int_pos_dep_col = self['colonne']

        #Variables de la position des pieces diagonales, retourne  ligne, colonne gauche, colonne droite,
        int_pos_dia = [(self['ligne']-1),(self['colonne']-1),(self['colonne']+1)] #La ligne est la meme pour les 2 positions

        'Création et remplissage du dictionaire'
        dic_pos_dia = {} #Création du dictionaire
        lst_pos_dia['bas gauche'] = {'ligne':int_pos_dia[0],'colonne':int_pos_dia[1]}#Diagonale gauche
        lst_pos_dia['bas droite'] = {'ligne':int_pos_dia[0],'colonne':int_pos_dia[2]}

        'Retour des variables'
        return(dic_pos_dia)

    def positions_diagonales_haut(self):
        """Retourne une liste contenant les deux positions diagonales haut à partir de la position actuelle.

        Returns:
            list: La liste des deux positions.

        """

        'Déclaration des variables'
        #Variables de la position de la piece de depart
        int_pos_dep_lig = self['ligne']
        int_pos_dep_col = self['colonne']

        #Variables de la position des pieces diagonales, retourne  ligne, colonne gauche, colonne droite,
        int_pos_dia = [(self['ligne']+1),(self['colonne']-1),(self['colonne']+1)] #La ligne est la meme pour les 2 positions

        'Création et remplissage du dictionaire'
        lst_pos_dia = {} #Création du dictionaire
        lst_pos_dia['haut gauche'] = {'ligne':int_pos_dia[0],'colonne':int_pos_dia[1]}#Diagonale gauche
        lst_pos_dia['haut droite'] = {'ligne':int_pos_dia[0],'colonne':int_pos_dia[2]}#Diagonale droite

        'Retour des variables'
        return(lst_pos_dia)

    def quatre_positions_diagonales(self):
        """Retourne une liste contenant les quatre positions diagonales à partir de la position actuelle.

        Returns:
            list: La liste des quatre positions.

        """

        'Déclaration de variables et assemblage'
        #Déclaration
        lst_pos_diag_bas = self.positions_diagonales_bas()
        lst_pos_diag_haut = self.positions_diagonales_haut()

        #Assemblage des deux dictionnaires
        dic_total = {} #Création de la liste
        dic_total.append(lst_pos_diag_bas) #Ajout des valeurs du bas
        dic_total.append(lst_pos_diag_haut) #Ajout des valeurs du haut

        #(lst_total ordre = [diag. gauche bas, diag. droite bas, diag. gauche haut, diag. droite haut]

        'Retour des variables'
        return(dic_total)

    def quatre_positions_sauts(self):
        """Retourne une liste contenant les quatre "sauts" diagonaux à partir de la position actuelle. Les positions
        retournées sont donc en diagonale avec la position actuelle, mais a une distance de 2.

        Returns:
            list: La liste des quatre positions.

        """

        'Déclaration de variables de départ'
        lst_quatre_pos_normal = self.quatre_positions_diagonales() #Création des quatres coins
        #Déclaration de variables de calcul.
        int_addition_ligne = -1 #Variable de changement de ligne
        int_addition_col = -1 #Variable de changement de colonne
        lst_quatre_pos_final = [] #Variable contenant le resultat final

        'Boucle qui modifie les valeurs pour chacune des coordonnées'
        for n in lst_quatre_pos_normal:
            lst_contenu = lst_quatre_pos_normal[n] #Assignation du point courant à une liste (Ligne, Colonne)
            if n >= 2: #Si les calculs sont rendus dans les coordonnées supérieures
                int_addition_ligne = 1
            if n % 2 == 0: #S'il s'agit des coordonnées paires (Les coordonnées paires sont à gauche)
                int_addition_col = -1
            else: #S'il s'agit de coordonnées impaires (À droite)
                int_addition_col = 1
            #Création et assignation d'une liste de contenu final
            lst_contenu_final = []
            lst_contenu_final[0] = (lst_contenu[0]+(int_addition_col))#Coordonnée X
            lst_contenu_final[1] = (lst_contenu[1]+(int_addition_ligne))#Coordonnée Y
            #Ajout de la valeur modifiée à la liste finale
            lst_quatre_pos_final[n]=lst_contenu_final

        #(lst_total ordre = [diag. gauche bas, diag. droite bas, diag. gauche haut, diag. droite haut]

        'Retour des variables'
        return(lst_quatre_pos_final)

    def __eq__(self, other):
        """Méthode spéciale indiquant à Python comment vérifier si deux positions sont égales. On compare simplement
        la ligne et la colonne de l'objet actuel et de l'autre objet.

        """
        return self.ligne == other.ligne and self.colonne == other.colonne

    def __repr__(self):
        """Méthode spéciale indiquant à Python comment représenter une instance de Position par une chaîne de
        caractères. Notamment utilisé pour imprimer une position à l'écran.

        """
        return '({}, {})'.format(self.ligne, self.colonne)

    def __hash__(self):
        """Méthode spéciale indiquant à Python comment "hasher" une Position. Cette méthode est nécessaire si on veut
        utiliser une classe que nous avons définie nous mêmes comme clé d'un dictionnaire.

        """
        return hash(str(self))